# Use Debian as the base image
FROM debian:bullseye as builder

# Install necessary dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    curl \
    && rm -rf /var/lib/apt/lists/*

# Install Rust toolchain
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y && \
    /root/.cargo/bin/rustup default stable

# Set the working directory
WORKDIR /usr/src/iproj2

# Copy the source code into the container
COPY . .

# Build the application
RUN /root/.cargo/bin/cargo build --release

# Final stage: Use a smaller image to run the executable
FROM debian:bullseye

# Set the working directory
WORKDIR /usr/src/iproj2

# Copy the built executable from the builder stage
COPY --from=builder /usr/src/iproj2/target/release/iproj2 .

# Expose the port on which the application will run
EXPOSE 8081

# Command to run the application
CMD ["./iproj2"]

