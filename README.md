# IDS721 Ip2


## Project steps
### Built and Ran the Service:
I used Cargo, Rust's package manager, to build and run the service locally.

### Tested the Service:
Utilized the Thunder Client extension in Visual Studio Code to test the REST API endpoints and ensure their functionality.
### Containerized the Service with Docker:
Created a Dockerfile for the project to containerize the service.
### Built a Docker image from the Dockerfile.
Ran a Docker container using the built image.
### Set Up CI/CD Pipeline with GitLab:
Used GitLab CI/CD to automate the build, test, and deployment processes.
Created a .gitlab-ci.yml file in the repository to define the CI/CD pipeline stages and jobs.
### Executed the Pipeline:
Triggered the CI/CD pipeline by pushing changes to the repository.
Monitored the pipeline execution to ensure successful completion of each stage and job.
